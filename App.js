document.addEventListener("DOMContentLoaded", function () {
  const loginContainer = document.getElementById("loginContainer");
  const signUpContainer = document.getElementById("signupContainer");
  const toggleSignUpButton = document.getElementById("toggleSignup");
  const toggleLoginButton = document.getElementById("toggleLogin");
  const signupButton = document.getElementById("signupButton");

  function toggleForms() {
    loginContainer.style.display =
      loginContainer.style.display === "none" ? "block" : "none";
    signUpContainer.style.display =
      signUpContainer.style.display === "none" ? "block" : "none";
  }
  toggleSignUpButton.addEventListener("click", function () {
    toggleForms();
  });

  toggleLoginButton.addEventListener("click", function () {
    toggleForms();
  });

  // Signup
  signupButton.addEventListener("click", function () {
    const username = document.getElementById("signupUsername").value;
    const email = document.getElementById("signupEmail").value;
    const password = document.getElementById("signupPassword").value;

    if (localStorage.getItem(username) || localStorage.getItem(email)) {
      alert("Username or email already exists");
    } else {
      localStorage.setItem(
        username,
        JSON.stringify({ username, email, password })
      );
      console.log("User signed up:", { username, email, password });
      alert("Signed Up Successfully!");
      return;
    }

    document.getElementById("signupUsername").value = "";
    document.getElementById("signupEmail").value = "";
    document.getElementById("signupPassword").value = "";
  });

  // Login
  const loginButton = document.getElementById("loginButton");
  loginButton.addEventListener("click", function () {
    const loginUsername = document.getElementById("loginUsername").value;
    const loginPassword = document.getElementById("loginPassword").value;

    const userData = JSON.parse(localStorage.getItem(loginUsername));
    if (userData && userData.password === loginPassword) {
      console.log("User logged in:", { username: loginUsername });
      alert("Login successful!");
      sessionStorage.setItem("loggedInUser", loginUsername);
      window.location.href = "Dashboard.html";
    } else {
      alert("Invalid username or password");
    }

    document.getElementById("loginUsername").value = "";
    document.getElementById("loginPassword").value = "";
  });
});

document.addEventListener("DOMContentLoaded", function () {
  const loggedInUser = sessionStorage.getItem("loggedInUser");

  if (loggedInUser) {
    const usernameContainer = document.getElementById("usernameContainer");
    usernameContainer.innerHTML = `<p>Hello, ${loggedInUser}!</p>`;
  }

  // Function to create buttons for a task and add event listeners
  function createTaskButtons(userData, li, task) {
    const buttonsDiv = document.createElement("div");
    buttonsDiv.classList.add("buttons");

    const deleteButton = document.createElement("span");
    deleteButton.classList.add("delete");
    deleteButton.textContent = "Delete";
    deleteButton.addEventListener("click", function () {
      const index = userData.tasks.findIndex(
        (t) => t.title === task.title && t.description === task.description
      );
      if (index !== -1) {
        userData.tasks.splice(index, 1);
        localStorage.setItem(loggedInUser, JSON.stringify(userData));
        li.remove();
        alert("Task deleted successfully!");
      }
    });

    const editButton = document.createElement("span");
    editButton.classList.add("edit");
    editButton.textContent = "Edit";
    editButton.addEventListener("click", function () {
      const newTitle = prompt("Enter the new title for the task:", task.title);
      const newDescription = prompt("Enter the new description for the task:", task.description);

      if (newTitle !== null && newDescription !== null) {
        task.title = newTitle;
        task.description = newDescription;
        localStorage.setItem(loggedInUser, JSON.stringify(userData));
        li.innerHTML = `<span class="title">${task.title}</span><span class="desc">${task.description}</span>`;
        createTaskButtons(userData, li, task); // Reattach event listeners after editing
        alert("Task edited successfully!");
      }
    });

    const doneButton = document.createElement("span");
    doneButton.classList.add("done");
    doneButton.textContent = "Done";
    doneButton.addEventListener("click", function () {
      const index = userData.tasks.findIndex(
        (t) => t.title === task.title && t.description === task.description
      );
      if (index !== -1) {
        const completedTask = userData.tasks.splice(index, 1)[0];
        if (!userData.completedTasks) {
          userData.completedTasks = []; // Create completedTasks array if it doesn't exist
        }
        userData.completedTasks.push(completedTask); // Add the completed task to completedTasks array
        localStorage.setItem(loggedInUser, JSON.stringify(userData)); // Save the updated userData
        li.removeChild(li.querySelector(".buttons"));
        li.remove();
      }
      completedTasks.appendChild(li); // Move the task to the "Completed Tasks" div
      alert("Task completed successfully!");
    });

    buttonsDiv.appendChild(deleteButton);
    buttonsDiv.appendChild(editButton);
    buttonsDiv.appendChild(doneButton);

    li.appendChild(buttonsDiv);
  }

  // Add Task
  const addButton = document.getElementById("addButton");

  addButton.addEventListener("click", function () {
    const title = document.getElementById("title").value;
    const description = document.getElementById("description").value;

    if (!title.trim() || !description.trim()) {
      alert("Please enter both title and description");
      return;
    }

    let userData = JSON.parse(localStorage.getItem(loggedInUser));
    if (!userData) {
      userData = {
        email: loggedInUser,
        tasks: [],
      };
    }

    const task = {
      title: title,
      description: description,
    };

    if (!userData.tasks) {
      userData.tasks = [];
    }

    userData.tasks.push(task);
    localStorage.setItem(loggedInUser, JSON.stringify(userData));

    document.getElementById("title").value = "";
    document.getElementById("description").value = "";

    const taskItems = document.getElementById("taskItems");
    const li = document.createElement("li");
    li.classList.add("listItems"); 
    li.innerHTML = `<span class="title">${task.title}</span><span class="desc">${task.description}</span>`;
    
    taskItems.appendChild(li);
    createTaskButtons(userData, li, task); // Attach event listeners to the newly added buttons

    alert("Task added successfully!");
  });

  // Display existing tasks on page load
  const taskItems = document.getElementById("taskItems");
  const completedTasks = document.getElementById("completedTasks"); 
  const userData = JSON.parse(localStorage.getItem(loggedInUser));
  if (userData && userData.tasks) {
    userData.tasks.forEach((task) => {
      const li = document.createElement("li");
      li.classList.add("listItems"); 
      li.innerHTML = `<span class="title">${task.title}</span><span class="desc">${task.description}</span>`;
      taskItems.appendChild(li);
      createTaskButtons(userData, li, task); // Attach event listeners to the existing buttons
      const doneButton = li.querySelector(".done");
      // doneButton.addEventListener("click", function () {
      //   const index = userData.tasks.findIndex(
      //     (t) => t.title === task.title && t.description === task.description
      //   );
      //   if (index !== -1) {
      //     const completedTask = userData.tasks.splice(index, 1)[0];
      //     if (!userData.completedTasks) {
      //       userData.completedTasks = []; // Create completedTasks array if it doesn't exist
      //     }
      //     userData.completedTasks.push(completedTask); // Add the completed task to completedTasks array
      //     localStorage.setItem(loggedInUser, JSON.stringify(userData)); // Save the updated userData
      //     li.removeChild(li.querySelector(".buttons"));
      //     li.remove();
      //   }
      //   completedTasks.appendChild(li); // Move the task to the "Completed Tasks" div
      //   alert("Task completed successfully!");
      // });
    });
  }
  if (userData && userData.completedTasks) {
    userData.completedTasks.forEach((task) => {
      const li = document.createElement("li");
      li.classList.add("listItems");
      li.innerHTML = `<span class="title">${task.title}</span><span class="desc">${task.description}</span>`;
      completedTasks.appendChild(li); // Append the completed task to the "completedTasks" div
    });
  }

});



